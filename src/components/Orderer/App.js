import React, { Component, PropTypes } from 'react';
import OrderMenu from './OrderMenu';
import OrderTicket from './OrderTicket';

import SizeSelector from './SizeSelector';
import ModifySelector from './ModifySelector';

import '../../styles/orderer.scss';

class OrderApp extends Component {
  constructor(props) {
    super(props);

    fetch('/api/foods')
      .then(response => response.json())
      .then((foods) => this.setState({ foods }));

    fetch('/api/extras')
      .then(response => response.json())
      .then((extras) => this.setState({ extras }));

    this.state = {
      currentOrder: {
        foods: [],
        notes: ""
      },
      selectedItemIndex: 0,
      isExtrasModeActive: false,
      foods: [],
      extras: []
    };
  }

  addItemToOrder = (item) => {
    this.setState({
      currentOrder: {
        foods: this.state.currentOrder.foods.concat([item]),
        notes: this.state.currentOrder.notes
      },
      selectedItemIndex: this.state.currentOrder.foods.length
    });
  };

  removeItemAtIndex = (index) => {
    this.setState({
      currentOrder: {
        foods: this.state.currentOrder.foods.filter((_, i) => i !== index),
        notes: this.state.currentOrder.notes
      },
      selectedItemIndex: Math.max(0, index - 1)
    });
  };

  addExtraToSelectedItem = (extra) => {
    if (extra.modify) {
      extra.name = `${extra.modify.toUpperCase()} ${extra.name}`;
    }
    this.setState({
      currentOrder: {
        foods: this.state.currentOrder.foods.map((food, i) => {
          if (i === this.state.selectedItemIndex) {
            let extras = food.extras || [];
            extras = extras.concat([extra]);
            return {
              ...food,
              extras
            };
          }
          return food;
        }),
        notes: this.state.currentOrder.notes
      },
      isExtrasModeActive: false
    }, () => console.log(this.state));
  };

  updateNotes = (notes) => {
    this.setState({
      currentOrder: {
        foods: this.state.currentOrder.foods,
        notes
      }
    });
  };

  sendOrder = () => {
    fetch('/api/order', {
      method: 'put',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(this.state.currentOrder)
    });

    this.clearOrder();
  };

  clearOrder = () => {
    this.setState({
      currentOrder: {
        foods: [],
        notes: ""
      },
      selectedItemIndex: 0,
      isExtrasModeActive: false
    });
  };

  updateSelectedItem = (selectedItemIndex) => {
    this.setState({ selectedItemIndex });
  };

  toggleExtrasMode = () => {
    this.setState({ isExtrasModeActive: !this.state.isExtrasModeActive });
  };

  render() {
    let menu;
    if (this.state.isExtrasModeActive) {
      menu = <OrderMenu foods={this.state.extras} selector={ModifySelector} handleSelect={this.addExtraToSelectedItem.bind(this)} />;
    } else {
      menu = <OrderMenu foods={this.state.foods} selector={SizeSelector} handleSelect={this.addItemToOrder.bind(this)} />;
    }
    return (
      <div id="order-app" className="flex">
        {menu}
        <div id="current-order" className="flex">
          <OrderTicket
            handleClick={this.updateSelectedItem.bind(this)}
            handleRemoveItemClick={this.removeItemAtIndex.bind(this)}
            handleExtrasClick={this.toggleExtrasMode.bind(this)}
            items={this.state.currentOrder.foods}
            selectedItemIndex={this.state.selectedItemIndex} />
          <textarea id="order-notes" placeholder="Order Notes" value={this.state.currentOrder.notes} onChange={(event) => this.updateNotes(event.target.value)}></textarea>
          <button id="submit-order" onClick={this.sendOrder.bind(this)} disabled={this.state.currentOrder.foods.length === 0}>Send</button>
          <button id="cancel-order" onClick={this.clearOrder.bind(this)}>Cancel</button>
        </div>
      </div>
    );
  }
}

export default OrderApp;
