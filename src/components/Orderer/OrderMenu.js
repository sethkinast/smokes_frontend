import React, { Component, PropTypes } from 'react';
import OrderMenuButton from './OrderMenuButton.js';

class OrderMenu extends Component {
  static propTypes = {
    foods: PropTypes.array.isRequired,
    handleSelect: PropTypes.func.isRequired,
    selector: PropTypes.func
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ul id="order-foods" className="flex">
        {this.props.foods.map(food => <OrderMenuButton {...food} selector={this.props.selector} key={food.id} handleSelect={this.props.handleSelect} />)}
      </ul>
    );
  }
}

export default OrderMenu;
