import React, { Component, PropTypes } from 'react';
import SizeSelector from './SizeSelector.js';

class OrderMenuButton extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    cid: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    handleSelect: PropTypes.func.isRequired,
    selector: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      isActive: false
    };
  }

  componentWillReceiveProps() {
    this.setState({ isActive: false });
  }

  handleClick = () => {
    if (this.props.selector) {
      this.setState({ isActive: true });
    } else {
      this.props.handleSelect(this.props.id);
    }
  };

  handleSelect = (item) => {
    this.props.handleSelect({
      id: this.props.id,
      name: this.props.name,
      ...item
    });
  };

  render() {
    let hue = this.props.cid * this.props.cid * 10;
    let style = { "backgroundColor" : `hsl(${hue},80%,70%)` };
    if (this.state.isActive) {
      return (
        <li style={style}>
          {React.createElement(this.props.selector, {
            handleSelect: this.handleSelect.bind(this),
            categoryID: this.props.cid
          })}
        </li>
      );
    }
    return <li style={style} onClick={this.handleClick.bind(this)}><span>{this.props.name}</span></li>;
  }
}

export default OrderMenuButton;
