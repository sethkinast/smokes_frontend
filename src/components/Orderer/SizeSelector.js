import React, { PropTypes } from 'react';

function SizeSelector(props) {
  let SIZES = [
    { size: 1, sizeName: 'Meal'},
    { size: 2, sizeName: 'Wow'}
  ];

  // Items in the basic category (Traditional, Fries, etc) have a Snack size
  if (props.categoryID === 0) {
    SIZES.unshift({ size: 0, sizeName: 'Snack' });
  }

  return (
    <div className="size-selector selector">
      {SIZES.map(({ size, sizeName }) => (
        <button
          className={`size-${sizeName.toLowerCase()}`}
          onClick={() => props.handleSelect({ size })}
          key={size}>
          {sizeName}
        </button>
      ))}
    </div>
  );
}

SizeSelector.propTypes = {
  handleSelect: PropTypes.func.isRequired,
  categoryID: PropTypes.number
};

export default SizeSelector;
