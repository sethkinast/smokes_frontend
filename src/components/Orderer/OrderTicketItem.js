import React, { Component, PropTypes } from 'react';
import OrderItem from '../shared/OrderItem.js';

class OrderTicketItem extends Component {
  static propTypes = {
    item: PropTypes.object.isRequired,
    isSelected: PropTypes.bool,
    handleClick: PropTypes.func.isRequired,
    handleRemoveItemClick: PropTypes.func.isRequired,
    handleExtrasClick: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
  }

  handleRemoveItemClick = (event) => {
    event.stopPropagation();
    this.props.handleRemoveItemClick();
  };

  handleExtrasClick = (event) => {
    event.stopPropagation();
    this.props.handleExtrasClick();
  };

  render() {
    let isSelected = this.props.isSelected;
    let buttons;
    if (isSelected) {
      buttons = (
        <div className="order-actions flex">
          <button onClick={this.handleExtrasClick} className="order-actions-extras">Modify</button>
          <button onClick={this.handleRemoveItemClick} className="order-actions-remove">Remove</button>
        </div>
      );
    }
    return (
      <li onClick={this.props.handleClick} className={this.props.isSelected ? "selected" : null}>
        <OrderItem {...this.props.item} />
        {buttons}
      </li>
    );
  }
}

export default OrderTicketItem;
