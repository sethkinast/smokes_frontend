import React, { Component, PropTypes } from 'react';
import OrderTicketItem from './OrderTicketItem.js';

class OrderTicket extends Component {
  static propTypes = {
    items: PropTypes.array.isRequired,
    selectedItemIndex: PropTypes.number.isRequired,
    handleClick: PropTypes.func.isRequired,
    handleRemoveItemClick: PropTypes.func.isRequired,
    handleExtrasClick: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ul id="order-ticket">
        {this.props.items.map((item, index) => <OrderTicketItem
          handleClick={this.props.handleClick.bind(this, index)}
          handleRemoveItemClick={this.props.handleRemoveItemClick.bind(this, index)}
          handleExtrasClick={this.props.handleExtrasClick}
          item={item}
          isSelected={index === this.props.selectedItemIndex}
          key={index} />)}
      </ul>
    );
  }
}

export default OrderTicket;
