import React, { PropTypes } from 'react';

const MODIFIERS = [
  { modify: "Add" },
  { modify: "Light" },
  { modify: "No" }
];

function ModifySelector(props) {
  return (
    <div className="selector modify-selector">
      {MODIFIERS.map((modifier, index) => (
        <button
          className={`modify-${modifier.modify.toLowerCase()}`}
          onClick={() => props.handleSelect(modifier)}
          key={index}>
          {modifier.modify}
        </button>
      ))}
    </div>
  );
}

ModifySelector.propTypes = {
  handleSelect: PropTypes.func.isRequired
};

export default ModifySelector;
