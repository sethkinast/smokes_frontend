import React, { Component, PropTypes } from 'react';
import Order from './Order.js';
import SoundBank from '../shared/SoundBank.js';

class RegisterApp extends Component {

  constructor(props) {
    super(props);
    this.state = { orders: [] };
  }

  componentDidMount() {
    this.fetchOrders();
    window.setInterval(() => this.fetchOrders(), 15000);
  }

  fetchOrders = () => {
    fetch('/api/orders')
      .then(response => response.json())
      .then((orders) => this.setState({ orders }));
  };

  render() {
    return (
      <div id="register" className="flex">
        {this.state.orders.filter((order) => !order.completed).length ? <SoundBank repeat={60} /> : false}
        {this.state.orders.map((order) => <Order {...order} key={order.id} />)}
      </div>
    );
  }
}

export default RegisterApp;
