import React, { Component, PropTypes } from 'react';
import OrderItem from '../shared/OrderItem';

class Order extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
    fetch(`/api/order/${props.id}`)
      .then(response => response.json())
      .then(data => this.setState({
        items: data.items,
        notes: data.order.notes,
        loaded: true,
        complete: data.order.completed
      }));

    this.state = { loaded: false, complete: false };
  }

  handleComplete = () => {
    fetch(`/api/order/${this.props.id}`, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        complete: true
      })
    });
    this.setState({ complete: true });
  };

  render() {
    if (!this.state.loaded) { return <div className="order">Loading...</div>; }

    let className = "order";

    if (this.state.complete) {
      className += ' complete';
    }

    let notes = this.state.notes ? <p>{this.state.notes}</p> : false;

    return (
      <div className={className}>
        <ul className="order-items">
          {this.state.items.map((item, i) => <li key={i}><OrderItem {...item} /></li>)}
        </ul>
        {notes}
        <button className="order-complete" onClick={this.handleComplete}>Complete</button>
      </div>
    );
  }
}

export default Order;
