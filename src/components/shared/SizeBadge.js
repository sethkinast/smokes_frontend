import React, { PropTypes } from 'react';
import '../../styles/size-badge.scss';

const SIZES = ['Snack', 'Meal', 'Wow'];

function SizeBadge(props) {
  let sizeName = SIZES[props.size];
  let classes = `size-badge size-${sizeName.toLowerCase()}`;
  return <span className={classes}>{sizeName}</span>;
}

SizeBadge.propTypes = {
  size: PropTypes.number.isRequired
};

export default SizeBadge;
