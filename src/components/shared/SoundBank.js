import React, { Component, PropTypes } from 'react';

const Sound = require('react-sound');
const BANK = [
  'https://wiki.teamfortress.com/w/images/8/8d/Heavy_incoming01.wav',
  'https://wiki.teamfortress.com/w/images/3/36/Heavy_incoming02.wav',
  'https://wiki.teamfortress.com/w/images/d/df/Heavy_incoming03.wav'
];

class SoundBank extends Component {
  static propTypes = {
    repeat: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.state = {
      url: this.pickSound(),
      isPlaying: true
    };

    if (props.repeat) {
      this.timer = setInterval(() => this.setState({ isPlaying: true }), props.repeat * 1000);
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  pickSound = () => BANK[Math.floor(Math.random() * BANK.length)];

  finished = () => {
    this.setState({
      isPlaying: false,
      url: this.pickSound()
    });
  };

  render() {
    return this.state.isPlaying ? <Sound url={this.state.url} onFinishedPlaying={this.finished} playStatus={Sound.status.PLAYING} /> : false;
  }
}

export default SoundBank;
