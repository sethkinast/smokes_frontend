import React, { PropTypes } from 'react';
import SizeBadge from '../shared/SizeBadge.js';

function OrderItem(props) {
  let extras;
  if (props.extras && props.extras.length) {
    let extrasArray = props.extras;
    if (typeof extrasArray === 'string') {
      extrasArray = extrasArray.split('|').map((name, index) => ({ name, id: index }));
    }
    extras = (<ul className="item-extras">
      {extrasArray.map((extra, i) => <li key={i}>{extra.name}</li>)}
    </ul>);
  }
  return (
    <div className="order-item">
      <SizeBadge size={props.size} />
      <span className="order-item-name">{props.name}</span>
      {extras}
    </div>
  );
}

OrderItem.propTypes = {
  extras: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ]),
  size: PropTypes.number.isRequired,
  name: PropTypes.string
};

export default OrderItem;
