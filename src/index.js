import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute } from 'react-router';
import { createHistory, useBasename } from 'history';
import fetch from 'whatwg-fetch';

import './styles/app.scss';
import OrderApp from './components/Orderer/App';
import RegisterApp from './components/Register/App';

const history = useBasename(createHistory)({
  basename: '/'
});

render((
  <Router history={history}>
    <Route path="/">
      <IndexRoute component={OrderApp} />
      <Route path="register" component={RegisterApp} />
    </Route>
  </Router>
), document.getElementById('app'));
